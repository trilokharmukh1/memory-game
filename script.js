const gameContainer = document.getElementById("game");
let clickFlag = 0;
let previousClass = ""
let previousId = "";
let count = 0;
let openCard = 0;
let openCardFlag = 0;
let moves = 0;
const bestMovesDiv = document.querySelector('.best-moves');
const movesDiv = document.querySelector('.moves');
movesDiv.innerHTML = `Your Moves: ${moves}`;


if (localStorage.bestMoves) {
  bestMovesDiv.innerHTML = `Best Moves : <span>${localStorage.bestMoves}<span>`;
}

function getValue() {
  console.log("select");
}

const images = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif",

];

function startGame() {
  document.querySelector('.game-section').style.display = "block";
  document.querySelector('.start-container').style.display = "none";
}
function restart() {
  document.querySelector('#game').style.opacity = 0.3;
  document.querySelector('#game').style.pointerEvents = "none";
  document.querySelector('.restart-section').style.display = "block";
}

function confirmRestart() {
  document.querySelector('#game').style.opacity = 1;
  document.querySelector('.restart-section').style.display = "none";
  document.querySelector('#game').style.pointerEvents = "auto";
  restartGame();
}

function cancleRestart() {
  document.querySelector('#game').style.opacity = 1;
  document.querySelector('.restart-section').style.display = "none";
  document.querySelector('#game').style.pointerEvents = "auto";
}

function restartGame() {
  clickFlag = 0;
  previousClass = "";
  previousId = "";
  count = 0;
  openCard = 0;
  openCardFlag = 0;
  moves = 0;
  movesDiv.innerHTML = `Your Moves: ${moves}`;
  gameContainer.innerHTML = "";
  let shuffledImages = shuffle(images);
  createDivsForImagess(shuffledImages);
}


function shuffle(array) {
  array = array.concat(array);
  let counter = array.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
let shuffledImages = shuffle(images);


function createDivsForImagess(images) {
  let count = 1;
  for (let image of images) {

    const newDiv = document.createElement("div");
    newDiv.setAttribute('class', `${image} cards`);
    newDiv.setAttribute('id', `card${count}`);
    newDiv.addEventListener("click", handleCardClick);

    const sectionOne = document.createElement("section");
    sectionOne.classList.add("front-face");

    let imageSection = document.createElement('img');
    imageSection.setAttribute('src', "./gifs/front-face.jpeg");
    sectionOne.append(imageSection);

    const sectionTwo = document.createElement("section");
    sectionTwo.classList.add("back-face");

    imageSection = document.createElement("img");
    imageSection.setAttribute("src", `${image}`);
    sectionTwo.append(imageSection);

    newDiv.append(sectionOne);
    newDiv.append(sectionTwo);
    gameContainer.append(newDiv);

    count++;
  }
}

// TODO: Implement this function!

function handleCardClick(event) {
  if (clickFlag > 0) {
    return;
  }

  openCardFlag = 1;
  event.target.closest('div').removeEventListener('click', handleCardClick);

  let card = event.target.closest('div');
  card.classList.toggle('flipCard');
  let className = card.classList[0];

  if (previousId == card.id) {
    return;
  }

  count++;

  if (count == 2) {

    clickFlag = 1;

    let previousCardDiv = document.getElementById(previousId);
    let previousCardClass = previousClass;
    let currentCardDiv = document.getElementById(card.id);

    if (previousCardClass != className) {
      setTimeout(() => {
        previousCardDiv.classList.toggle('flipCard');
        currentCardDiv.classList.toggle('flipCard');

        clickFlag = 0;
        previousClass = "";
        previousId = "";
        openCardFlag = 0;
        previousCardDiv.addEventListener('click', handleCardClick);
        currentCardDiv.addEventListener('click', handleCardClick);

      }, 1000)

    } else {

      previousClass = "";
      previousId = "";
      clickFlag = 0;
      openCard += 2;

      previousCardDiv.removeEventListener('click', handleCardClick);
      currentCardDiv.removeEventListener('click', handleCardClick);

    }

    count = 0;
  }

  if (count == 1) {
    previousId = card.id;
    previousClass = className;
  }

  moves++;
  movesDiv.innerHTML = `Your Moves : ${moves}`

  if (openCard == images.length * 2) {
    bestMoves();
  }

}

function bestMoves() {
  if (localStorage.bestMoves) {
    if (moves < +localStorage.bestMoves) {
      localStorage.bestMoves = moves;
      console.log(moves);
    }
  } else {
    localStorage.setItem('bestMoves', moves)
    console.log(moves);
  }
  bestMovesDiv.innerHTML = `Best Moves : <span>${localStorage.bestMoves}<span>`;

}

createDivsForImagess(shuffledImages);


